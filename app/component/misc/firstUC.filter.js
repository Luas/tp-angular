var app = angular.module("myApp");

app.filter('firstUC',function(){
	return function(input){
		var first = input.charAt(0).toUpperCase();
		var reste = input.slice(1);
		return first + reste;
	}
})