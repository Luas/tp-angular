var homeMod = angular.module("home", ['ui.router']);

homeMod.config(function($stateProvider){
	$stateProvider
		.state(
			'home',
			{
				url: "/home",
				templateUrl: "app/component/home/home.html",
				controller: 'homeCtrl',
				controllerAs: 'home'
			}
		);
});
homeMod.controller('homeCtrl', function(){
	this.name = "Alexandre Nguyen";
});