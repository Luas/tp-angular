var heroMod = angular.module('heroes');

heroMod.directive('heroCard', function(){
	return {
		restrict: 'E',
		templateUrl: 'app/component/heroes/hero-card.html',
		scope: {
			heroDetail: '=heroData'
		},
		controller: 'heroDirectiveCtrl',
		bindToController: 'heroDirectiveCtrl'
	};
});

heroMod.controller('heroDirectiveCtrl', function(){
});