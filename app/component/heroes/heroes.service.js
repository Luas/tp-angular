var heroMod = angular.module('heroes');

heroMod.service('heroesSrv', function($http){
	this.getHeroes = function(){
		return $http.get('app/heroes.json')
			.then(function(res){
				return res.data;
			});
	};
});