var heroMod = angular.module('heroes', ['ui.router']);

heroMod.config(function($stateProvider){
	$stateProvider
		.state(
			'heroes',
			{
				url: "/heroes",
				templateUrl: "app/component/heroes/heroes.html",
				controller: 'heroesController',
				controllerAs: 'heroesCtrl'
			}
		);
});

heroMod.controller('heroesController', function(heroesSrv){
	var self = this;
	heroesSrv.getHeroes().then(function(res){
		self.heroes = res;
	});
});