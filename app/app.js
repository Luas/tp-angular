var app = angular.module("myApp", ['ui.router', 'heroes', 'home']);

app.config(function($urlRouterProvider){

	$urlRouterProvider.otherwise("/home");
});